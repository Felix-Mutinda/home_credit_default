from airflow.models import Variable
import os

from home_credit_default.utils.utils import generate_dag_run_path, extract_repo_name

# [Define custom variables]
project_args = {
    'ts': "{{ ts_nodash }}",  # execution date in '20180101T000000' format
    'airflow_run_dir': os.getenv('AIRFLOW_RUN_DIR'),
    'repo_name': extract_repo_name(Variable.get('HOME_CREDIT_REPO_URL')),
    'repo_url': Variable.get('HOME_CREDIT_REPO_URL'),
    'repo_username': Variable.get('HOME_CREDIT_REPO_USERNAME'),
    'repo_password': Variable.get('HOME_CREDIT_REPO_PASSWORD'),
    'branch': Variable.get('HOME_CREDIT_BRANCH'),
    'storage_path': os.getenv('HOME_CREDIT_STORAGE'),
    'dvc_storage': os.getenv('HOME_CREDIT_DVC_STORAGE')
}

project_args['dag_run_dir'] = generate_dag_run_path(
    project_args['airflow_run_dir'],
    project_args['repo_name'],
    ts_macros=project_args['ts']
)
