import argparse
import joblib
import pandas as pd
from pathlib import Path
from typing import Text, Any

from src.utils.config import load_config
from src.utils.logging import get_logger
from src.data.features import get_one_hot_encoder, encode_cat_features, get_aggregated_features


def calculate_bureau_features(bureau_data: pd.DataFrame, config: Any, 
                              logger: Any) -> pd.DataFrame:
    """Calculate Bureau data features.

    Args:
        bureau_data (pd.DataFrame): Bureau data
        config (Dict): Pipeline config (params.yaml)
        logger (Any): Stage logger

    Returns:
        pd.DataFrame: Features with 'SK_ID_CURR' set as index
    """
    
    logger.info('Calculate features -> Load encoder')
    dir_models = Path(config.base.dir_models)
    encoder_path = dir_models / config.extract_features.bureau.encoder_path
    bureau_ohe = joblib.load(encoder_path)
    
    logger.info('Calculate features -> Encode Categorical features')
    bureau_cat_encoded = encode_cat_features(
        df=bureau_data, 
        cat_features=config.extract_features.bureau.cat_names,
        encoder=bureau_ohe, index_cols=['SK_ID_CURR'])
    
    logger.info('Calculate features -> Calculate Categorical features')
    bureau_features_cat_sum = get_aggregated_features(
        df=bureau_cat_encoded, 
        feature_names=bureau_ohe.get_feature_names_out(),
        agg_func='sum', 
        index_cols=['SK_ID_CURR'])

    bureau_features_cat_mean = get_aggregated_features(
        df=bureau_cat_encoded,
        feature_names=bureau_ohe.get_feature_names_out(),
        agg_func='mean',
        index_cols=['SK_ID_CURR'])
    
    logger.info('Calculate features -> Calculate Numerical features')
    bureau_features_num_sum = get_aggregated_features(
        df=bureau_data,
        feature_names=config.extract_features.bureau.num_names,
        agg_func='sum',
        index_cols=['SK_ID_CURR'])

    bureau_features_num_mean = get_aggregated_features(
        df=bureau_data, 
        feature_names=config.extract_features.bureau.num_names,
        agg_func='mean', 
        index_cols=['SK_ID_CURR'])
    
    logger.info('Calculate features -> Join features ')
    features = pd.concat([
        bureau_features_cat_sum,
        bureau_features_cat_mean,
        bureau_features_num_sum,
        bureau_features_num_mean],
        axis=1)
    logger.info(f'Calculate features -> Features shape: {features.shape}')
    
    return features


def features_extract_bureau(config_path: Text) -> None:
    """Calculate features for Bureau data.
    
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    logger = get_logger("FEATURES_BUREAU", config.base.log_level)
    dir_data_raw = Path(config.data.raw.dir_path)
    dir_data_processed = Path(config.data.processed.dir_path)
    dir_models = Path(config.base.dir_models)

    logger.info('Load dataset')
    bureau_data = pd.read_csv(dir_data_raw / config.data.raw.bureau_data_path)

    logger.info('Fit Onehot encoder')
    bureau_cat_features = config.extract_features.bureau.cat_names
    bureau_ohe = get_one_hot_encoder(df=bureau_data, 
                                     cat_features=bureau_cat_features)
    encoder_path = dir_models / config.extract_features.bureau.encoder_path
    joblib.dump(bureau_ohe, encoder_path)
    logger.info(f'Save encoder to: {encoder_path}')

    logger.info('Calculate features')
    features = calculate_bureau_features(bureau_data=bureau_data,
                                         config=config,
                                         logger=logger)
    
    logger.info('Save features')
    features_path = dir_data_processed / config.extract_features.bureau.features_path
    features.to_csv(features_path)
    logger.info(f'Save processed features to: {features_path}')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    features_extract_bureau(config_path=args.config)