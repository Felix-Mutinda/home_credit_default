import argparse
import os
import pandas as pd
from pathlib import Path
from typing import Text

from src.utils.config import load_config
from src.utils.logging import get_logger


def load_data(config_path: Text, storage_path: Text) -> None:
    """Load and process data.
    
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    logger = get_logger("LOAD_DATA", config.base.log_level)

    if not storage_path:
        logger.warning(f'Storage path is not set. Default value will be used')
        storage_path = './data'

    dir_data_raw = Path(config.data.raw.dir_path)
    storage_path = Path(storage_path)
    logger.info(f'Storage path = {storage_path}')
    dir_data_storage = storage_path / config.data.raw.storage_dir_path

    logger.info('Load data')
    bureau_data = pd.read_csv(dir_data_storage / config.data.raw.bureau_data_path)
    app_data = pd.read_csv(dir_data_storage / config.data.raw.application_data_path)
    
    logger.info('Sample data')
    bureau_data = bureau_data.sample(frac=config.load_data.sample_size, random_state=config.base.random_state)
    app_data = app_data.sample(frac=config.load_data.sample_size, random_state=config.base.random_state)

    logger.info('Save data')
    bureau_data_path = dir_data_raw / config.data.raw.bureau_data_path
    bureau_data.to_csv(bureau_data_path, index=False)
    app_data_path = dir_data_raw / config.data.raw.application_data_path
    app_data.to_csv(app_data_path, index=False)
    logger.info('Save data complete')
    
    logger.info('Save data Index')
    bureau_index_path = dir_data_raw / config.data.raw.bureau_index_path
    bureau_data[['SK_ID_CURR', 'SK_ID_BUREAU']].to_csv(bureau_index_path, index=False)
    app_index_path = dir_data_raw / config.data.raw.application_index_path
    app_data[['SK_ID_CURR', 'TARGET', 'MONTH', 'DATE']].to_csv(app_index_path, index=False)
    logger.info('Save index complete')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args_parser.add_argument('--storage-path', dest='storage_path', default=None)
    args = args_parser.parse_args()

    load_data(config_path=args.config, storage_path=args.storage_path)
