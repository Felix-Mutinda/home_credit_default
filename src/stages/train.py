import argparse
import catboost as ctb
import json
from mlem.api import save
import pandas as pd
from pathlib import Path
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from typing import Text

from src.evaluate.metrics import precision_at_k_score, recall_at_k_score, lift_score
from src.train.train import custom_ts_split, get_split_data
from src.utils.config import load_config
from src.utils.errors import UnknownEstimatorError
from src.utils.logging import get_logger
from src.utils.plot import build_lift_curve, plot_confusion_matrix


def train(config_path: Text) -> None:
    """Train model.
    
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    logger = get_logger("TRAIN", config.base.log_level)
    dir_data_processed = Path(config.data.processed.dir_path)
    dir_reports = Path(config.base.dir_reports)
    estimator_name = config.train.estimator
    estimator_params = config.train.estimators[estimator_name]
    random_state = config.base.random_state

    logger.info('Load data')
    data_train = pd.read_csv(dir_data_processed / config.extract_features.join_features.features_path)
    data_train['MONTH'] = pd.to_datetime(data_train['MONTH'])
    
    logger.info('Initialize Estimator')
    if estimator_name == 'catboost':
        estimator = ctb.CatBoostClassifier
    elif estimator_name == 'random_forest':
        estimator = RandomForestClassifier
    else:
        raise UnknownEstimatorError(f'Unknown estimator name: {estimator_name}')
    
    clf = estimator(**estimator_params, random_state=random_state)
    logger.info(f'Top_K is {config.metrics.top_K_coef * 100} % of dataset_size: {data_train.shape}')
    top_K = int(data_train.shape[0] * config.metrics.top_K_coef)
    metrics_df = pd.DataFrame(columns=['test_period', 'lift', 'precision_at_k', 'recall_at_k'])
    months = data_train['MONTH'].sort_values().unique()
    k = 1
    y_test = []
    y_pred = []
    sample_data = pd.DataFrame()

    logger.info('Train model')
    for start_train, end_train, test_period in custom_ts_split(months, train_period=1):
        logger.info(f'Fold {k}:')
        logger.info(f'Train: {start_train} - {end_train}')
        logger.info(f'Test: {test_period} \n')

        # Get train / test data for the split
        X_train, X_test, y_train, y_test = get_split_data(data_train, start_train, end_train, test_period)
        logger.info(f'Train shapes: X - {X_train.shape}, y - {y_train.shape}')
        logger.info(f'Test shapes: X - {X_test.shape}, y - {y_test.shape}')

        sample_data = X_train

        # Fit estimator
        clf.fit(X_train, y_train)

        y_pred = clf.predict(X_test)
        probas = clf.predict_proba(X_test)
        logger.info(f'Max probas: {probas[:, 1].max()}')

        lift = lift_score(y_test, y_pred, probas[:, 1], top_K)
        precision_at_k = precision_at_k_score(y_test, y_pred, probas[:, 1], top_K)
        recall_at_k = recall_at_k_score(y_test, y_pred, probas[:, 1], top_K)
        metrics_df = metrics_df.append(
            dict(zip(metrics_df.columns, [test_period, lift, precision_at_k, recall_at_k])),
            ignore_index=True
        )

        k += 1
        logger.info(f'Precision at {top_K}: {precision_at_k}')
        logger.info(f'Recall at {top_K}: {recall_at_k}\n')
    
    logger.info('Save model')
    logger.info('Save model')
    mlem_model_path = config.train.mlem_model_path
    save(
        clf,
        mlem_model_path,
        sample_data=sample_data
    )
    logger.info(f'Model saved to: {mlem_model_path}')
    
    logger.info('Save Lift Curve plot')
    lift_curve_fig = build_lift_curve(y_test[:top_K], y_pred[:top_K], step=config.metrics.lift_curve_step)
    lift_curve_fig.savefig(dir_reports / config.plots.lift_curve_image)

    logger.info('Save "raw" metrics (for plotting)')
    metrics_df.to_csv(dir_reports / config.metrics.raw_metrics_path, index=False)

    logger.info('Save aggregated metrics')
    metrics_aggs = metrics_df[['lift', 'precision_at_k', 'recall_at_k']].agg(['max', 'min', 'std', 'mean'])
    metrics = {
        f'{metric}_{agg}': metrics_aggs.loc[agg, metric]
        for metric in metrics_aggs.columns
        for agg in metrics_aggs.index
    }
    with open(dir_reports / config.metrics.agg_metrics_path, 'w') as metrics_f:
        json.dump(obj=metrics, fp=metrics_f, indent=4)
    
    logger.info('Save confusion matrix (last test period)')
    cm = confusion_matrix(y_pred, y_test)
    plt = plot_confusion_matrix(cm=cm, target_names=['0', '1'], normalize=False)
    confusion_matrix_png_path = dir_reports / config.plots.confusion_matrix_path
    plt.savefig(confusion_matrix_png_path)
    logger.info(f'Confusion matrix saved to : {confusion_matrix_png_path}')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    train(config_path=args.config)
