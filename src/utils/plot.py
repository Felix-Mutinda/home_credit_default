import itertools
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
import numpy as np
import pandas as pd
from typing import Iterable, List, Text


def pixels_in_inches(pixels: int = 1) -> float:
    """Converts pixels to inches bases on.
    
    Args:
        pixels {int}: pixels number
    
    Returns:
        float: value in inches
    """
    return pixels / plt.rcParams['figure.dpi']


def build_lift_curve(
        y_true: Iterable, y_pred: Iterable, step: float = 0.01,
        width: int = 600, height: int = 400
) -> Figure:
    """Function that builds a Lift Curve Figure using the real label values of a dataset and
    the probability predictions of a Machine Learning Algorithm/model.
    
    Args:
        y_true {Iterable}: real labels of the data
        y_pred {Iterable}: probability predictions for such data
        step {float}: how big we want the steps in the percentiles to be
        width {int}: image width in pixels
        height {int}: image height in pixels
        
    Reference: https://towardsdatascience.com/the-lift-curve-unveiled-998851147871
    """

    # Define an auxiliar dataframe to plot the curve
    aux_lift = pd.DataFrame()
    # Create a real and predicted column for our new DataFrame and assign values
    aux_lift['real'] = y_true
    aux_lift['predicted'] = y_pred
    # Order the values for the predicted probability column:
    aux_lift.sort_values('predicted', ascending=False, inplace=True)

    # Create the values that will go into the X axis of our plot
    x_val = np.arange(step, 1 + step, step)
    # Calculate the ratio of ones in our data
    ratio_ones = aux_lift['real'].sum() / len(aux_lift)
    # Create an empty vector with the values that will go on the Y axis our our plot
    y_v = []

    # Calculate for each x value its correspondent y value
    for x in x_val:
        num_data = int(
            np.ceil(x * len(aux_lift)))  # The ceil function returns the closest integer bigger than our number
        data_here = aux_lift.iloc[:num_data, :]  # ie. np.ceil(1.4) = 2
        ratio_ones_here = data_here['real'].sum() / len(data_here)
        y_v.append(ratio_ones_here / ratio_ones)

    # Calc figure sizes
    px = pixels_in_inches()
    fig_width = width * px
    fig_height = height * px

    # Build the figure
    fig, axis = plt.subplots(figsize=(fig_width, fig_height))
    axis.plot(x_val, y_v, 'g-', linewidth=3, markersize=5)
    axis.plot(x_val, np.ones(len(x_val)), 'k-')
    axis.set_xlabel('Proportion of sample')
    axis.set_ylabel('Lift')
    axis.set_title('Lift Curve')

    return fig


def plot_lift_curve(
        y_true: Iterable, y_pred: Iterable, step: float = 0.01,
        width: int = 600, height: int = 400
) -> None:
    """Function that plots a Lift Curve using the real label values of a dataset and
    the probability predictions of a Machine Learning Algorithm/model.
    
    Args:
        y_true {Iterable}: real labels of the data
        y_pred {Iterable}: probability predictions for such data
        step {float}: how big we want the steps in the percentiles to be
        width {int}: image width in pixels
        height {int}: image height in pixels
    
    Reference: https://towardsdatascience.com/the-lift-curve-unveiled-998851147871
    """

    build_lift_curve(y_true, y_pred, step, width, height)
    plt.show()


def plot_confusion_matrix(cm: np.array,
                          target_names: List[Text],
                          title: Text = 'Confusion matrix',
                          cmap: matplotlib.colors.LinearSegmentedColormap = None,
                          normalize: bool = True):
    """Given a sklearn confusion matrix (cm), make a nice plot.

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """

    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))

    return plt.gcf()