from typing import List


def rename_columns(colnames: List, prefix: str = None, postfix: str = None, 
                   exclude: List = None) -> List:
    """Rename columns with prefix/postfix specified.

    Args:
        colnames (List): List of column names (original)
        prefix (str, optional): Apply this prefix at the beginning of the colname. Defaults to None.
        postfix (str, optional): Apply this postfix at the end of the colname. Defaults to None.
        exclude (List, optional): List of colnames to not change. Defaults to None.

    Returns:
        List: New list of column names
    """    

    if not exclude:
        exclude = []

    new_list = []

    for col in colnames:
        
        if col in exclude: 
            new_list.append(col)
        
        else:
            new_name = col
            
            if prefix:
                new_name = f'{prefix}_{new_name}'
                
            if postfix:
                new_name = f'{new_name}_{postfix}'
                
            new_list.append(new_name)
            
    assert len(new_list) == len(colnames)
    
    return new_list
