from src.utils.utils import rename_columns

def test_rename_columns():
    
    case_1 = rename_columns(['id', 'colA', 'colB'], prefix='prefix', exclude=['id'])
    assert case_1 == ['id', 'prefix_colA', 'prefix_colB'], print(case_1)
    
    case_2 = rename_columns(['id', 'colA', 'colB'], prefix='prefix', postfix='postfix', exclude=['colB'])
    assert case_2 == ['prefix_id_postfix', 'prefix_colA_postfix', 'colB'], print(case_2)
    
    case_3 = rename_columns(['id', 'colA', 'colB'], prefix='prefix', postfix='postfix')
    assert case_3 == ['prefix_id_postfix', 'prefix_colA_postfix', 'prefix_colB_postfix'], print(case_3)